# -*- coding: utf-8 -*-

{
    'name': 'Flow Payment Acquirer',
    'category': 'Payment / Chile',
    'author': 'Daniel Santibáñez Polanco',
    'summary': 'Payment Acquirer: Chilean Flow Payment Acquirer',
    'website': 'https://globalresponse.cl',
    'version': "0.7.2",
    'description': """Chilean Flow Payment Acquirer""",
    'depends': [
            'payment',
            'payment_currency',
    ],
    'external_dependencies': {
            'python': [
                'payflow',
            ],
    },
    'data': [
        'views/flow.xml',
        'views/payment_provider.xml',
        'views/payment_transaction.xml',
        'data/flow.xml',
    ],
    'post_init_hook': 'post_init_hook',
    'uninstall_hook': 'uninstall_hook',
    'installable': True,
    'application': True,
}
