# -*- coding: utf-8 -*-
import json
import pprint
import requests
import werkzeug
from werkzeug import urls
from odoo import http
from odoo.http import request
from markupsafe import Markup
import logging


_logger = logging.getLogger(__name__)

try:
    import urllib3
    pool = urllib3.PoolManager()
except:
    pass


class FlowController(http.Controller):
    _accept_url = '/payment/flow/test/accept'
    _decline_url = '/payment/flow/test/decline'
    _exception_url = '/payment/flow/test/exception'
    _cancel_url = '/payment/flow/test/cancel'

    @http.route([
        '/payment/flow/notify/<int:provider_id>',
        '/payment/flow/test/notify',
    ], type='http', auth='public', methods=['POST'], csrf=False, sitemap=False)
    def flow_validate_data(self, provider_id=None, **post):
        _logger.info("notify %s, provider_id %s" %(post, provider_id))
        provider = request.env['payment.provider'].sudo().browse(int(provider_id))
        tx_data = provider.sudo().flow_getTransaction(post)
        tx_sudo = request.env['payment.transaction'].sudo()._get_tx_from_notification_data('flow', tx_data)
        tx_sudo.operation = 'online_redirect'
        if not tx_sudo:
            raise ValidationError("Transacción no esperada")
        return ''

    @http.route([
        '/payment/flow/return/<int:provider_id>',
        '/payment/flow/test/return',
    ], type='http', auth='public', csrf=False, sitemap=False)
    def flow_form_feedback(self, provider_id=None, **post):
        provider = request.env['payment.provider'].sudo().browse(int(provider_id))
        _logger.info("post %s, provider_id %s" %(post, provider_id))
        if not provider:
            return
        try:
            tx_data = provider.flow_getTransaction(post)
            tx_data._token = post['token']
            request.env['payment.transaction'].sudo()._handle_notification_data('flow', tx_data)
        except:
            pass
        return werkzeug.utils.redirect('/payment/status')

    @http.route([
        '/payment/flow/final',
        '/payment/flow/test/final',
    ], type='http', auth='none', csrf=False, website=True, sitemap=False)
    def final(self, **post):
        return werkzeug.utils.redirect('/payment/process')

    @http.route(['/payment/flow/redirect/<model("payment.transaction"):tx_id>'],
        type='http', auth='public', methods=["POST"], csrf=False, website=True)
    def redirect_flow(self, tx_id=None, **post):
        result = tx_id._flow_send_payment_request()
        if result.token:
            return werkzeug.utils.redirect(result.url+'?token='+result.token)
        #@TODO render error
        values={
            '': '',
        }
        #return request.render('payment_flow.flow_redirect', values)
