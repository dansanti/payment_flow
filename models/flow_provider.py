# -*- coding: utf-'8' "-*-"
import time
from odoo.addons.payment import utils as payment_utils
from datetime import datetime, timedelta
from odoo import api, models, fields
from odoo.tools import float_round, DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.float_utils import float_compare, float_repr
from odoo.tools.safe_eval import safe_eval
from odoo.tools.translate import _
from werkzeug import urls
import logging
_logger = logging.getLogger(__name__)

try:
    from payflow.client import Client
except Exception as e:
    _logger.warning("No se puede cargar Flow: %s" %str(e), exc_info=True)



class PaymentProviderFlow(models.Model):
    _inherit = 'payment.provider'

    code = fields.Selection(
            selection_add=[('flow', 'Flow')],
            ondelete={'flow': 'set default'}
        )
    flow_api_key = fields.Char(
            string="Api Key",
        )
    flow_private_key = fields.Char(
            string="Secret Key",
        )
    flow_payment_method = fields.Selection(
        [
            ('1', 'Webpay'),
            ('2', 'Servipag'),
            ('3', 'Multicaja'),
            ('5', 'Onepay'),
            ('8', 'Cryptocompra'),
            ('9', 'Todos los medios'),
        ],
        required=True,
        default='1',
    )

    def _get_supported_currencies(self):
        supported_currencies = super()._get_supported_currencies()
        if self.code == 'flow':
            supported_currencies = supported_currencies.filtered(
                lambda c: c.name in const.SUPPORTED_CURRENCIES
            )
        return supported_currencies

    def _get_default_payment_method_codes(self):
        """ Override of `payment` to return the default payment method codes. """
        default_codes = super()._get_default_payment_method_codes()
        if self.code != 'flow':
            return default_codes
        return const.DEFAULT_PAYMENT_METHOD_CODES

    def _get_feature_support(self):
        res = super(PaymentProviderFlow, self)._get_feature_support()
        res['fees'].append('flow')
        return res

    def flow_compute_fees(self, amount, currency_id, country_id):
        """ Compute Flow fees.

            :param float amount: the amount to pay
            :param integer country_id: an ID of a res.country, or None. This is
                                       the customer's country, to be compared to
                                       the acquirer company country.
            :return float fees: computed fees
        """
        if not self.fees_active:
            return 0.0
        country = self.env['res.country'].browse(country_id)
        if country and self.company_id.country_id.id == country.id:
            percentage = self.fees_dom_var
            fixed = self.fees_dom_fixed
        else:
            percentage = self.fees_int_var
            fixed = self.fees_int_fixed
        factor = (percentage / 100.0) + (0.19 * (percentage /100.0))
        fees = ((amount + fixed) / (1 - factor))
        return (fees - amount)

    @api.model
    def _get_flow_urls(self, environment):
        base_url = self.env['ir.config_parameter'].sudo().get_param(
                    'web.base.url')
        if environment == 'prod':
            return {
                'flow_form_url': base_url + '/payment/flow/redirect',
                'flow_url': "https://www.flow.cl/api",
            }
        else:
            return {
                'flow_form_url': base_url + '/payment/flow/redirect',
                'flow_url': "https://sandbox.flow.cl/api",
            }

    def flow_form_generate_values(self, values):
        #banks = self.flow_get_banks()#@TODO mostrar listados de bancos
        #_logger.warning("banks %s" %banks)
        values.update({
            'acquirer_id': self.id,
            'commerceOrder': values['reference'].split('-')[0],
            'subject': values['reference'],
            'amount': values['amount'],
            'email': values['partner_email'],
            'paymentMethod': self.flow_payment_method,
            #'fees': values.get('fees', 0),
        })
        return values

    def flow_get_form_action_url(self):
        environment = 'prod' if self.state == 'enabled' else 'test'
        return self._get_flow_urls(environment)['flow_form_url']

    def flow_get_client(self,):
        environment = 'prod' if self.state == 'enabled' else 'test'
        return Client(
                self.flow_api_key,
                self.flow_private_key,
                self._get_flow_urls(environment)['flow_url'],
                (environment == 'test'),
            )

    def flow_get_banks(self):
        client = self.flow_get_client()
        return client.banks.get()

    def _flow_make_request(self, payload=None, method="GET"):
        base_url = self.get_base_url()
        amount = float(payload['amount']) #+ float(payload.get('fees', 0.00)))
        currency = self.env['res.currency'].search([
            ('name', '=', payload.get('currency_code', 'CLP')),
        ])
        if amount < 350:
            raise ValidationError("Monto total no debe ser menor a $350")
        payload.update({
                    'paymentMethod': str(payload.get('paymentMethod')),
                    'urlConfirmation': base_url + f'/payment/flow/notify/{self.id}',
                    'urlReturn': base_url + f'payment/flow/return/{self.id}',
                    'currency': currency.name,
                    'amount': str(currency.round(amount)),
                    })
        #payload['uf'] += '/%s' % str(self.id)
        client = self.flow_get_client()
        _logger.info(f"Datos a pagar {payload}")
        res = client.payments.post(payload)
        if hasattr(res, 'payment_url'):
            tx.write({'state': 'pending'})
        return res

    def flow_getTransaction(self, post):
        client = self.flow_get_client()
        return client.payments.get(post['token'])


class PaymentTxFlow(models.Model):
    _inherit = 'payment.transaction'

    flow_token = fields.Char(
        string="Flow Token Transaction",
    )

    @api.model
    def _compute_reference(self, provider, prefix=None, separator='-', **kwargs):
        """ Override of payment to ensure that Flow requirements for references are satisfied.
        Flow requirements for references are as follows:
        - References must be unique at provider level for a given merchant account.
          This is satisfied by singularizing the prefix with the current datetime. If two
          transactions are created simultaneously, `_compute_reference` ensures the uniqueness of
          references by suffixing a sequence number.
        :param str provider: The provider of the provider handling the transaction
        :param str prefix: The custom prefix used to compute the full reference
        :param str separator: The custom separator used to separate the prefix from the suffix
        :return: The unique reference for the transaction
        :rtype: str
        """
        if provider != 'flow':
            return super()._compute_reference(provider, prefix=prefix, **kwargs)

        if not prefix:
            # If no prefix is provided, it could mean that a module has passed a kwarg intended for
            # the `_compute_reference_prefix` method, as it is only called if the prefix is empty.
            # We call it manually here because singularizing the prefix would generate a default
            # value if it was empty, hence preventing the method from ever being called and the
            # transaction from received a reference named after the related document.
            prefix = self.sudo()._compute_reference_prefix(provider, separator, **kwargs) or None
        prefix = payment_utils.singularize_reference_prefix(prefix=prefix, max_length=40)
        return super()._compute_reference(provider, prefix=prefix, **kwargs)

    def _get_specific_rendering_values(self, processing_values):
        """ Override of payment to return Flow-specific rendering values.
        Note: self.ensure_one() from `_get_processing_values`
        :param dict processing_values: The generic and specific processing values of the transaction
        :return: The dict of provider-specific processing values
        :rtype: dict
        """
        res = super()._get_specific_rendering_values(processing_values)
        if self.provider_code != 'flow':
            return res
        provider = self.provider_id.sudo()
        base_url = provider.get_base_url()
        return_url = urls.url_join(base_url, '/payment/flow/final')
        api_url = urls.url_join(base_url, f'/payment/flow/redirect/{self.id}')
        processing_values.update({
            'currency_code': self.currency_id.name,
            'commerceOrder': self.reference.split('-')[0],
            'subject': self.reference,
            'amount': self.amount,
            'email': self.partner_email,
            'paymentMethod': provider.flow_payment_method,
            #'fees': self.fees,
            'api_url': api_url,
            'return_url': return_url,
            'tx_id': self.id,
        })
        return processing_values

    def _flow_send_payment_request(self):
        provider = self.provider_id.sudo()
        base_url = provider.get_base_url()
        return_url = urls.url_join(base_url, '/payment/flow/final')
        data = {
            'business': self.company_id.name,
            'commerceOrder': self.reference.split('-')[0],
            'subject': self.reference,
            'amount': self.amount,
            'currency_code': self.currency_id.name,
            'address1': self.partner_address or '',
            'city': self.partner_city or '',
            'country': self.partner_country_id.code or '',
            'email': self.partner_email or '',
            'return_url': return_url,
            'paymentMethod': provider.flow_payment_method,
        }
        return provider._flow_make_request(data)

    def _flow_form_get_invalid_parameters(self, data):
        invalid_parameters = []

        if data.subject != self.reference:
            invalid_parameters.append(('reference', data.subject, self.reference))
        # check what is buyed
        amount = self.amount# + self.fees)
        currency = self.currency_id
        if self.provider_id.force_currency and currency != self.provider_id.force_currency_id:
            amount = lambda price: currency._convert(
                                amount,
                                self.provider_id.force_currency_id,
                                self.provider_id.company_id,
                                datetime.now())
            currency = self.provider_id.force_currency_id
        amount = currency.round(amount)
        if float(data.amount) != amount:
            invalid_parameters.append(('amount', data.amount, amount))

        return invalid_parameters

    def _get_tx_from_notification_data(self, provider_code, notification_data):
        """ Override of payment to find the transaction based on Flow data.

        :param str provider_code: The code of the provider that handled the transaction.
        :param dict notification_data: The notification data sent by the provider.
        :return: The transaction if found.
        :rtype: recordset of `payment.transaction`
        :raise ValidationError: If inconsistent data were received.
        :raise ValidationError: If the data match no transaction.
        """
        tx = super()._get_tx_from_notification_data(provider_code, notification_data)
        if provider_code != 'flow' or len(tx) == 1:
            return tx
        reference, txn_id = notification_data.subject, notification_data.payment_id
        if not reference or not txn_id:
            error_msg = _('Flow: received data with missing reference (%s) or txn_id (%s)') % (reference, txn_id)
            _logger.warning(error_msg)
            raise ValidationError(error_msg)

        # find tx -> @TDENOTE use txn_id ?
        tx = self.search([
            ('reference', '=', reference),
            ('provider_code', '=', 'flow')],
            limit=1)
        if not tx or len(tx) > 1:
            error_msg = 'Flow: received data for reference %s' % (reference)
            if not tx:
                error_msg += '; no order found'
            else:
                error_msg += '; multiple order found'
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        return tx

    def _process_notification_data(self, notification_data):
        """ Override of payment to process the transaction based on Flow data.

        Note: self.ensure_one()

        :param dict notification_data: The notification data sent by the provider.
        :return: None
        :raise ValidationError: If inconsistent data were received.
        """
        super()._process_notification_data(notification_data)
        if self.provider_code != 'flow':
            return
        codes = {
                '0': 'Transacción aprobada.',
                '-1': 'Rechazo de transacción.',
                '-2': 'Transacción debe reintentarse.',
                '-3': 'Error en transacción.',
                '-4': 'Rechazo de transacción.',
                '-5': 'Rechazo por error de tasa.',
                '-6': 'Excede cupo máximo mensual.',
                '-7': 'Excede límite diario por transacción.',
                '-8': 'Rubro no autorizado.',
            }
        status = notification_data.status
        res = {
            'provider_reference': notification_data.payment_id,
            'flow_token': notification_data.token,
            #'fees': notification_data.payment_data['fee'],
        }
        if status in [2]:
            _logger.info('Validated flow payment for tx %s: set as done' % (self.reference))
            self._set_done()
            return self.write(res)
        elif status in [1, '-7']:
            _logger.warning('Received notification for flow payment %s: set as pending' % (self.reference))
            self._set_pending()
            return self.write(res)
        else: #3 y 4
            error = 'Received unrecognized status for flow payment %s: %s, set as error' % (self.reference, codes[status].decode('utf-8'))
            _logger.warning(error)
            return self.write(res)

    def flow_getTransactionfromCommerceId(self):
        notification_data = {
            'commerceId': self.reference.split('-')[0],
        }
        client = self.provider_id.flow_get_client()
        tx_data = client.payments.get_from_commerce_id(notification_data)
        self.sudo()._handle_notification_data('flow', tx_data)
        return self._finalize_post_processing()

'''
    def _get_specific_processing_values(self, processing_values):
        """ Override of payment to return Adyen-specific processing values.

        Note: self.ensure_one() from `_get_processing_values`

        :param dict processing_values: The generic processing values of the transaction
        :return: The dict of provider-specific processing values
        :rtype: dict
        """
        res = super()._get_specific_processing_values(processing_values)
        if self.provider_code != 'flow':
            return res

        converted_amount = payment_utils.to_minor_currency_units(
            self.amount, self.currency_id, const.CURRENCY_DECIMALS.get(self.currency_id.name)
        )
        return {
            'converted_amount': converted_amount,
            'access_token': payment_utils.generate_access_token(
                processing_values['reference'],
                converted_amount,
                self.currency_id.id,
                processing_values['partner_id']
            )
        }
'''
